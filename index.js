const express = require('express')
const parser = require('./helpers/parser.js')
const app = express()
const port = 9500


app.use(express.json())
app.use(express.urlencoded({ limit: '50mb', extended: true }))
app.use(express.static('pages', {
    extensions: ['txt','css']
}))

app.get('/',async (req,res)=>{
    const content = await parser.loadFile(`http://localhost:9500/p_home.txt`)
    res.set('Content-Type: text/html')
    res.send(content)
})

app.get('/:page',async (req,res)=>{
        const content = await parser.loadFile(`http://localhost:9500/p_${req.params.page}.txt`)
        res.set('Content-Type: text/html')
        res.send(content)
})

app.get('/api/:api',(req,res)=>{
    const mw = parser.loadMiddleware(`/api/${req.params.api}`)
    if(mw == null){ 
        const edge = require(`./pages/api/${req.params.api}.js`)
        const data = {
            message: 'success',
            data: edge(req,null)
        }
        res.set('Content-Type: application/json')
        res.status(200).json(data)
    } else {
        const ware = require(`./pages/middleware/${mw}.js`)
        if(!ware(req)){
            res.set('Content-Type: application/json')
            res.status(403).json({message: 'unauthorized'})
        } else {
            const edge = require(`./pages/api/${req.params.api}.js`)
            const data = {
                message: 'success',
                data: edge(req,ware(req))
            }
            res.set('Content-Type: application/json')
            res.status(200).json(data)
        }
    }
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })
const fetch = require('cross-fetch')
const middleware = require('../pages/middleware.js')

class parser {
    async loadFile(path){
        const data = await fetch(path)
        const content = await data.text()
        return content
    }
    loadMiddleware(route){
        if(middleware.length == 0){
            return null
        } else { 
            for(let item of middleware){
                if(item.route == route){
                    return item.middleware
                    break
                }
            }
        }
    }
}

module.exports = new parser